<!-- Form input type text and password -->
<!DOCTYPE html>
<html>
<head>
    <title><h3>Form input type TEXT dan PASSWORD</h3></title>
</head>
<body>
    <form action="" method="post" name="input">
        Daftar Program Studi FTI <br>
        1. <input type="text" name="prodi1"><br>
        2. <input type="text" name="prodi2"><br>
        3. <input type="text" name="prodi3"><br>
        <input type="submit" name="Input" value="Submit">/>
    </form>
</body>
</html>

<?php
if (isset($_POST['input'])){
    $prodi1 = $_POST['prodi1'];
    $prodi2 = $_POST['prodi2'];
    $prodi3 = $_POST['prodi3'];

    echo "<b> Daftar Nama Prodi FTI : </b><br>";
    echo "1) $prodi1 <br>";
    echo "2) $prodi2 <br>";
    echo "3) $prodi3 <br>";
}
?>


<!-- Form input type radio -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type RADIO</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Pilih Kebutuhan Anda Sekarang</h2>
        <input type="radio" name="sembako" value="BERAS"> Beras <br/>
        <input type="radio" name="sembako" value="MINYAK"> Minyak <br/>
        <input type="radio" name="sembako" value="TELUR"> Telur <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
if (isset($_POST['sembako'])){
    $sembako = $_POST['sembako'];
    echo "Sembako yang telah anda pilih adalah : <b><font color='blue'> $sembako </font><b>";
}
?>


<!-- Form input type checkbox -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type checkbox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Pilih hewan yang menyusui / mamalia</h2>
        <input type="checkbox" name="mamalia1" value="SAPI"> Sapi <br/>
        <input type="checkbox" name="mamalia2" value="ULAR"> Ular <br/>
        <input type="checkbox" name="mamalia3" value="KUDA"> Kuda <br/>
        <input type="submit" name="Input" value="Pilih">
    </form>
</body>
</html>

<?php
    echo "Yang merupakan hewan mamalia adalah : <br>";
    if (isset($_POST['mamalia1'])){
        echo "- " . $_POST['mamalia1'] . "<br>";
    }
    if (isset($_POST['mamalia2'])){
        echo "- " . $_POST['mamalia2'] . "<br>";
    }
    if (isset($_POST['mamalia3'])){
        echo "- " . $_POST['mamalia3'] . "<br>";
    }
?>


<!-- Form input type combobox -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type combobox</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="pilih">
        <h2> Pilih buah kesukaan anda</h2>
        <select name="buah">
            <option value="Durian">Durian</option>
            <option value="Semangka">Semangka</option>
            <option value="Rambutan">Rambutan</option>
            <option value="Nanas">Nanas</option>
        </select>
        <input type="submit" name="Pilih" value="Pilih">
</body>
</html>

<?php
    if (isset($_POST['buah'])){
        $buah = $_POST['buah'];
        echo "Buah favoritmu adalah : $buah <br>";
    }
?>


<!-- Form input type text area -->
<!DOCTYPE html>
<html>
<head>
    <title>Form input type text area</title>
</head>
<body>
    <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post" name="tulis">
        <h2> Biodata Saya </h2>
        <textarea name="biodata" cols="40" rows="6"></textarea><br>
        <input type="submit" name="tulis" value="Input Biodata">
</body>
</html>

<?php
    if (isset($_POST['tulis'])){
        $biodata = nl2br($_POST['biodata']);
        echo "Berikut adalah biodata saya : <br>";
        echo "$biodata";
    }
?>


<!-- Form input type Form Validation -->
<!DOCTYPE html>
<html>
<head>
    <title> Form Validation </title>
    <style>.error {color: #FF0000;}</style>
</head>
<body>

<?php
$namaErr = "";
$nama = "";

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if (empty($_POST["nama"])){
        $namaErr = "Nama harus diisi!";
    } else{
        $nama = test_input($_POST["nama"]);
        if (!preg_match("/^[a-zA-Z]*$/", $nama)){
            $namaErr = "Only letters and white space allowed";
        }
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <h2> Validasi Form </h2>
    <p><span class="error">* Masukkan nama.</span></p>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        Nama : <input type="text" name="nama" value="<?php echo $nama;?>">
        <span class="error">* <?php echo $namaErr;?></span>
        <br><br>
        <input type="submit" name="submit" value="Submit">
    </form>

<?php
echo "<h2> Hasil inputan : </h2>";
echo $nama;
echo "<br>"
?>

</body>
</html>