<!-- Array dengan FOR dan FOREACH -->
<html>
<head>
<title>Array for dan foreach</title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<!-- Array foreach dan while list -->
<html>
<head>
<title>Array foreach dan while list</title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
$nama = array(416=>"Laras", 430=>"Dola", 456=>"Sasqia", 452=>"Nanda", 417=>"Fajrial", 431=>"Prima", 443=>"Januar", 449=>"Krisna", 433=>"Aditya", 439=>"Rico");
echo "Menampilkan isi array asosiatif dengan FOREACH : <br>";
foreach ($nama as $NIM=>$isi){
    echo "Urutan NIM ke-$NIM adalah $isi<br>";
}
?>


<!-- Array dengan sort() dan rsort() -->
<html>
<head>
<title> Array dengan sort() dan rsort() </title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
$arrnama = array(416=>"Laras", 430=>"Dola", 456=>"Sasqia", 452=>"Nanda", 417=>"Fajrial", 431=>"Prima", 443=>"Januar", 449=>"Krisna");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($nama);
echo "</pre>";

sort($arrnama);
reset($arrnama);
echo "<b> Array setelah diurutkan dengan sort : <b>";
echo "<pre>";
print_r($arrnama);
echo "</pre>";

rsort($arrnama);
reset($arrnama);
echo "<b> Array setelah diurutkan dengan rsort : <b>";
echo "<pre>";
print_r($arrnama);
echo "</pre>";
?>


<!-- Array dengan asort() dan arsort() -->
<html>
<head>
<title> Array dengan asort() dan arsort() </title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
$arrNIM = array(416=>"Laras", 430=>"Dola", 456=>"Sasqia", 452=>"Nanda", 417=>"Fajrial", 431=>"Prima", 443=>"Januar", 449=>"Krisna");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";

asort($arrNIM);
reset($arrNIM);
echo "<b> Array setelah diurutkan dengan asort : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";

arsort($arrNIM);
reset($arrNIM);
echo "<b> Array setelah diurutkan dengan arsort : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";
?>


<!-- Array dengan ksort() dan krsort() -->
<html>
<head>
<title> Array dengan ksort() dan krsort() </title>
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
$arrNIM = array(416=>"Laras", 430=>"Dola", 456=>"Sasqia", 452=>"Nanda", 417=>"Fajrial", 431=>"Prima", 443=>"Januar", 449=>"Krisna");
echo "<b> Array sebelum diurutkan  : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";

ksort($arrNIM);
reset($arrNIM);
echo "<b> Array setelah diurutkan dengan ksort : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";

krsort($arrNIM);
reset($arrNIM);
echo "<b> Array setelah diurutkan dengan krsort : <b>";
echo "<pre>";
print_r($arrNIM);
echo "</pre>";
?>


<!-- Fungsi tampa return value dan parameter -->
<html>
<head>
<title> Fungsi tampa return value dan parameter </title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
// Fungsi ini ntanpa return value dan tanpa parameter!
function pesan(){
    echo "Nama saya Laras Yuan Indrawati. Biasa dipanggil Laras. Asal saya dari Kuningan, Jawa Barat. Saya lahir pada tanggal 18 Januari 2002. Saya sedang menempuh pendidikan prodi Informatika di Universitas Ahmad Dahlan.";
}
// Pemanggilan fungsi
pesan();
?>


<!-- Fungsi tampa return value tetapi dengan parameter -->
<html>
<head>
<title> Fungsi tampa return value tetapi dengan parameter </title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>

<?php
// Fungsi ini ntanpa return value tetapi dengan parameter parameter!
function perkenalan($nama, $salam="Assalamualaikum"){
    echo $salam.", ";
    echo "Perkenalkan, nama saya ".$nama."<br/>";
    echo "Nice to meet you <br/>";
}
// Pemanggilan fungsi
echo "<hr>";
perkenalan("Laras Yuan Indrawati");
echo "<hr>";
?>


<!-- Fungsi dengan return value dan parameter -->
<html>
<head>
<title> Fungsi dengan return value dan parameter </title>
<style type="text/css">
</style>
</head>
<body background="https://img.freepik.com/free-vector/cute-brown-aesthetic-abstract-minimal-background-perfect-wallpaper-backdrop-postcard-background_565280-444.jpg">
</body>
</html>